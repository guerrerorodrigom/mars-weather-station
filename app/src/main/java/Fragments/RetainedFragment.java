package Fragments;

import android.app.Fragment;
import android.os.Bundle;


import java.util.List;

import Models.MarsReport;
import Tasks.ITask;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class RetainedFragment extends Fragment {
    private ITask taskToExecute;
    private List<MarsReport> reportList;

    public List<MarsReport> getReportList() {
        return reportList;
    }

    public void setReportList(List<MarsReport> reportList) {
        this.reportList = reportList;
    }

    public ITask getTaskToExecute() {
        return taskToExecute;
    }

    public void setTaskToExecute(ITask taskToExecute) {
        this.taskToExecute = taskToExecute;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setTask(ITask taskToExecute){
        this.taskToExecute = taskToExecute;
    }

    public ITask getTask(){
       return taskToExecute;
    }
}
