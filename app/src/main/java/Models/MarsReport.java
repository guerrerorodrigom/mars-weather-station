package Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class MarsReport implements Serializable{

    @SerializedName("terrestrial_date")
    private String terrestrialDate;
    private int sol;
    @SerializedName("ls")
    private int marsSeason;
    @SerializedName("min_temp")
    private double minTemperatureCelsius;
    @SerializedName("min_temp_fahrenheit")
    private double minTemperatureFahrenheit;
    @SerializedName("max_temp")
    private double maxTemperatureCelsius;
    @SerializedName("max_temp_fahrenheit")
    private double maxTemperatureFahrenheit;
    private int pressure;
    @SerializedName("pressure_string")
    private String pressureString;
    @SerializedName("abs_humidity")
    private int humidity;
    @SerializedName("wind_speed")
    private int windSpeed;
    @SerializedName("wind_direction")
    private String windDirection;
    @SerializedName("atmo_opacity")
    private String atmosphericOpacity;
    private String season;
    private String sunrise;
    private String sunset;

    public MarsReport(){

    }

    public String getTerrestrialDate() {
        return terrestrialDate;
    }

    public void setTerrestrialDate(String terrestrialDate) {
        this.terrestrialDate = terrestrialDate;
    }

    public int getSol() {
        return sol;
    }

    public void setSol(int sol) {
        this.sol = sol;
    }

    public int getMarsSeason() {
        return marsSeason;
    }

    public void setMarsSeason(int marsSeason) {
        this.marsSeason = marsSeason;
    }

    public double getMinTemperatureCelsius() {
        return minTemperatureCelsius;
    }

    public void setMinTemperatureCelsius(double minTemperatureCelcius) {
        this.minTemperatureCelsius = minTemperatureCelcius;
    }

    public double getMinTemperatureFahrenheit() {
        return minTemperatureFahrenheit;
    }

    public void setMinTemperatureFahrenheit(double minTemperatureFahrenheit) {
        this.minTemperatureFahrenheit = minTemperatureFahrenheit;
    }

    public double getMaxTemperatureCelsius() {
        return maxTemperatureCelsius;
    }

    public void setMaxTemperatureCelsius(double maxTemperatureCelcius) {
        this.maxTemperatureCelsius = maxTemperatureCelcius;
    }

    public double getMaxTemperatureFahrenheit() {
        return maxTemperatureFahrenheit;
    }

    public void setMaxTemperatureFahrenheit(int maxTemperatureFahrenheit) {
        this.maxTemperatureFahrenheit = maxTemperatureFahrenheit;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public String getPressureString() {
        return pressureString;
    }

    public void setPressureString(String pressureString) {
        this.pressureString = pressureString;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getAtmosphericOpacity() {
        return atmosphericOpacity;
    }

    public void setAtmosphericOpacity(String atmosphericOpacity) {
        this.atmosphericOpacity = atmosphericOpacity;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getSunrise() {

        return sunrise;
    }

    private String getTime(String sunriseSunset){
        String[] components = sunriseSunset.split("T");
        if(components != null && components.length > 0){
            return components[1].substring(0, components[1].length() - 1);
        }

        return sunriseSunset;
    }

    public String getSunriseTime(){
        return getTime(sunrise);
    }

    public String getSunsetTime(){
        return getTime(sunset);
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }


    public void setSunset(String sunset) {
        this.sunset = sunset;
    }


}
