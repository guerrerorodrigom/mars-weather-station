package connect;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class ApiCalls {

    public static final String BASE_URL = "http://marsweather.ingenology.com/v1/";
    public static final String GET_LATEST_REPORT = "latest/";
    public static final String GET_ARCHIVE = "archive/";
}
