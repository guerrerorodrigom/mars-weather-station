package connect;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class GetReport extends HttpConnect {

    public GetReport(){
        super(ApiCalls.GET_LATEST_REPORT, HttpVerbs.Get);
    }
}
