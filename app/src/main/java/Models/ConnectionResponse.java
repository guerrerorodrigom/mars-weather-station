package Models;

import java.sql.Connection;

/**
 * Created by rodrigo.guerrero on 4/22/2015.
 */
public class ConnectionResponse {
    private int status;
    private String response;

    public ConnectionResponse(){}

    public ConnectionResponse(int status){
        this.status = status;
    }

    public ConnectionResponse(int status, String response){
        this(status);
        this.response = response;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
