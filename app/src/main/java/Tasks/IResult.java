package Tasks;

import java.util.List;

import Models.MarsReport;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public interface IResult {
    void SetPreExecuteTasks();
    void Result(List<MarsReport> reports);
}
