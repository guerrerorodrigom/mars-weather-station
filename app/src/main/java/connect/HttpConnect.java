package connect;

import org.apache.http.NameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import Models.ConnectionResponse;
import utils.Utils;

/**
 * Created by rodrigo.guerrero on 4/22/2015.
 */
public class HttpConnect {

    public enum HttpVerbs { Get, Post }
    private String url;
    public HashMap<String, String> parameters;
    private HttpVerbs httpVerb;

    public HttpConnect(String url, HttpVerbs httpVerb){
        this.url = ApiCalls.BASE_URL + url;
        this.httpVerb = httpVerb;
    }

    public ConnectionResponse execute() throws Exception{
        switch(this.httpVerb){
            case Get:
                return doGet(new URL(url), parameters);
            case Post:
                return doPost(new URL(url), parameters);
            default:
                return new ConnectionResponse(405);
        }
    }

    private ConnectionResponse doPost(URL url, HashMap<String, String> parameters){
        try {
            return downloadUrl(url, HttpVerbs.Post, parameters);
        }catch(IOException ex){
            return new ConnectionResponse(400); // TODO: RETURN ERROR CODE.
        }
    }

    private ConnectionResponse doGet(URL url, HashMap<String, String> parameters){
        URL urlQueryString = null;

        if(parameters != null && parameters.size() > 0){
            String urlString = url.toString() + "?";
            urlString = formatUrl(urlString, parameters);
            try {
                urlQueryString = new URL(urlString);
            }catch(MalformedURLException ex){
                urlQueryString = url;
            }
        }else{
            urlQueryString = url;
        }

        return doGet(urlQueryString);
    }

    private ConnectionResponse doGet(URL url){
        try {
            return downloadUrl(url, HttpVerbs.Get, null);
        }catch(IOException ex){
            return new ConnectionResponse(400); // TODO: RETURN ERROR CODE.
        }
    }

    private String formatUrl(String url, HashMap<String, String> parameters){
        StringBuilder result = new StringBuilder(url);
        String paramsString;

        if(parameters != null && parameters.size() > 0){
            for(String key : parameters.keySet()){
                String value = parameters.get(key);
                result.append(key);
                result.append("=");
                result.append(value);
                result.append("&");
            }

            paramsString = result.toString().substring(0, result.length() - 1);
        }else{
            paramsString = result.toString();
        }

        return paramsString;
    }

    private ConnectionResponse downloadUrl(URL url, HttpVerbs verb, HashMap<String, String> params) throws IOException{
        InputStream is = null;
        ConnectionResponse response = new ConnectionResponse();

        try{
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(Utils.READ_TIMEOUT);
            conn.setConnectTimeout(Utils.CONNECTION_TIMEOUT);
            conn.setRequestMethod(verb.toString().toUpperCase());
            conn.setDoInput(true);

            if(verb == HttpVerbs.Post){
                conn.setDoOutput(true);
                if(params != null && params.size() > 0){
                    String parameters = formatUrl("", params);
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                    writer.write(parameters);
                    writer.flush();
                    writer.close();
                    os.close();
                }
            }

            conn.connect();
            response.setStatus(conn.getResponseCode());
            is = conn.getInputStream();
            response.setResponse(convertInputStreamToString(is));
        }finally{
            if(is != null)
                is.close();
        }

        return response;
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;

        while(((line = reader.readLine()) != null)){
            sb.append(line);
        }

        reader.close();
        return sb.toString();
    }
}
