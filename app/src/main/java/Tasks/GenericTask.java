package Tasks;

import android.os.AsyncTask;

import Models.ConnectionResponse;
import connect.HttpConnect;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class GenericTask extends AsyncTask<HttpConnect, Void, ConnectionResponse> {

    private ITaskResult processResult;

    public GenericTask(ITaskResult processResult){
        this.processResult = processResult;
    }

    @Override
    protected void onPreExecute() {
        processResult.SetPreExecuteTasks();
    }

    @Override
    protected ConnectionResponse doInBackground(HttpConnect... params) {
        ConnectionResponse response = new ConnectionResponse();
        try {
            if (params != null && params.length > 0) {
                response = params[0].execute();
            }
        }catch(Exception ex){
            response.setStatus(400);
        }
        return response;
    }

    @Override
    protected void onPostExecute(ConnectionResponse connectionResponse) {
        this.processResult.ProcessResult(connectionResponse);
    }
}
