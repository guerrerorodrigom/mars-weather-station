package Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import Models.MarsReport;
import Tasks.GetLatestReportTask;
import Tasks.IResult;
import minds.warrior.com.marsweather.R;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class LastReportFragment extends Fragment implements IResult {
    private RetainedFragment retainedFragment;
    private final String retainedFragmentTag = "GetLatestReportTask";
    private GetLatestReportTask getLatestReportTask;

    private TextView tvMinTemp, tvMaxTemp, tvMinTempUnits, tvMaxTempUnits, tvPressure, tvPressureString, tvAtmo, tvSunrise, tvSunset;
    private TextView tvEarthDate, tvMarsDate;
    private CardView cvTemp, cvPressure, cvAtmo, cvSun, cvSunset, cvData;
    private LinearLayout lyCards;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_report, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvMinTemp = (TextView) view.findViewById(R.id.tvMinTemp);
        tvMaxTemp = (TextView) view.findViewById(R.id.tvMaxTemp);
        tvMinTempUnits = (TextView) view.findViewById(R.id.tvMinTempUnits);
        tvMaxTempUnits = (TextView) view.findViewById(R.id.tvMaxTempUnits);
        tvPressure = (TextView) view.findViewById(R.id.tvPressure);
        tvPressureString = (TextView) view.findViewById(R.id.tvPressureString);
        tvAtmo = (TextView) view.findViewById(R.id.tvAtmosphere);
        tvSunrise = (TextView) view.findViewById(R.id.tvSunriseSunset);
        tvSunset = (TextView) view.findViewById(R.id.tvSunset);
        tvEarthDate = (TextView) view.findViewById(R.id.tvEarthDate);
        tvMarsDate = (TextView) view.findViewById(R.id.tvMarsDate);

        cvTemp = (CardView) view.findViewById(R.id.cardTemperature);
        cvPressure = (CardView) view.findViewById(R.id.cardPressure);
        cvSun = (CardView) view.findViewById(R.id.cardSun);
        cvAtmo = (CardView) view.findViewById(R.id.cardAtmosphere);
        cvSunset = (CardView) view.findViewById(R.id.cardSunSet);
        cvData = (CardView) view.findViewById(R.id.cardGeneralData);
        lyCards = (LinearLayout) view.findViewById(R.id.lyReportCards);

        cvTemp.setCardBackgroundColor(getResources().getColor(R.color.card_bg));
        cvPressure.setCardBackgroundColor(getResources().getColor(R.color.card_bg));
        cvSun.setCardBackgroundColor(getResources().getColor(R.color.card_bg));
        cvAtmo.setCardBackgroundColor(getResources().getColor(R.color.card_bg));
        cvSunset.setCardBackgroundColor(getResources().getColor(R.color.card_bg));
        cvData.setCardBackgroundColor(getResources().getColor(R.color.card_bg));

        cvTemp.setRadius(25);
        cvPressure.setRadius(25);
        cvSun.setRadius(25);
        cvAtmo.setRadius(25);
        cvSunset.setRadius(25);
        cvData.setRadius(25);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

    }

    @Override
    public void onResume() {
        super.onResume();
        handleConfigChanges();
    }

    private void handleConfigChanges(){
        FragmentManager fm = getActivity().getFragmentManager();
        retainedFragment = (RetainedFragment) fm.findFragmentByTag(retainedFragmentTag);
        getLatestReportTask = new GetLatestReportTask(this);
        if(retainedFragment == null){
            retainedFragment = new RetainedFragment();
            fm.beginTransaction().add(retainedFragment, retainedFragmentTag).commit();
            retainedFragment.setTask(getLatestReportTask);

            ConnectivityManager connMgr = (ConnectivityManager)
                    getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            if(networkInfo != null && networkInfo.isConnected())
                ((GetLatestReportTask)retainedFragment.getTask()).execute();
            else
                Result(null);
        }else{
            Result(retainedFragment.getReportList());
        }
    }

    @Override
    public void SetPreExecuteTasks(){
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void Result(List<MarsReport> reportList){
        progressBar.setVisibility(View.GONE);
        if(reportList != null && reportList.size() > 0){
            retainedFragment.setReportList(reportList);
            lyCards.setVisibility(View.VISIBLE);
            MarsReport report = reportList.get(0);
            saveInSharedPreferences(report);
            fillInformation(report);
        }else{
            // Load from shared prefs.
            MarsReport report = getFromSharedPreferences();
            if(report != null) {
                lyCards.setVisibility(View.VISIBLE);
                fillInformation(report);
            }else{
                Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void fillInformation(MarsReport report){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String tempUnit = pref.getString("temperature_unit_list", getString(R.string.celsius_value));
        double minTemperature, maxTemperature;

        if(tempUnit.equals(getString(R.string.celsius_value))){
            minTemperature = report.getMinTemperatureCelsius();
            maxTemperature = report.getMaxTemperatureCelsius();
            tvMaxTempUnits.setText(getString(R.string.celsius));
            tvMinTempUnits.setText(getString(R.string.celsius));
        }else{
            minTemperature = report.getMinTemperatureFahrenheit();
            maxTemperature = report.getMaxTemperatureFahrenheit();
            tvMaxTempUnits.setText(getString(R.string.fahrenheit));
            tvMinTempUnits.setText(getString(R.string.fahrenheit));
        }

        tvMinTemp.setText(String.valueOf(minTemperature));
        tvMaxTemp.setText(String.valueOf(maxTemperature));
        tvPressure.setText(String.valueOf(report.getPressure()));
        tvPressureString.setText(report.getPressureString());
        tvAtmo.setText(report.getAtmosphericOpacity());
        tvSunrise.setText(report.getSunriseTime());
        tvSunset.setText(report.getSunsetTime());
        tvEarthDate.setText(report.getTerrestrialDate());
        tvMarsDate.setText(report.getSeason() + "\nLS " + report.getMarsSeason() + "°\nSol " + report.getSol());
    }

    private void saveInSharedPreferences(MarsReport report){
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String jsonObject = gson.toJson(report);
        editor.putString("lastReport", jsonObject);
        editor.commit();
    }

    private MarsReport getFromSharedPreferences(){
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("lastReport", "");

        if(json == null || json.equals(""))
            return null;
        else{
            MarsReport report = gson.fromJson(json, MarsReport.class);
            return report;
        }
    }
}
