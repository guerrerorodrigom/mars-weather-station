package Tasks;

import Models.ConnectionResponse;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public interface ITaskResult {
    void ProcessResult(ConnectionResponse response);
    void SetPreExecuteTasks();
}
