package Tasks;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import Models.ConnectionResponse;
import Models.MarsReport;
import connect.GetReport;
import connect.HttpConnect;

/**
 * Created by rodrigo.guerrero on 4/23/2015.
 */
public class GetLatestReportTask implements ITask, ITaskResult {

    private HttpConnect apiCall;
    private GenericTask task;
    private IResult processResult;

    public GetLatestReportTask(IResult processResult){
        this.apiCall = new GetReport();
        this.task = new GenericTask(this);
        this.processResult = processResult;
    }

    public void execute(){
        this.task.execute(apiCall);
    }

    @Override
    public void ProcessResult(ConnectionResponse response) {
        List<MarsReport> reportList = new ArrayList<>();

        if(response.getStatus() == 200){
            Gson gson = new Gson();
            JsonObject jsonObject = new Gson().fromJson(response.getResponse(), JsonObject.class);
            JsonElement jsonElement = jsonObject.get("report");
            MarsReport report = gson.fromJson(jsonElement, MarsReport.class);
            reportList.add(report);
        }

        this.processResult.Result(reportList);
    }

    @Override
    public void SetPreExecuteTasks() {
        processResult.SetPreExecuteTasks();
    }
}
